#!/usr/bin/env python

from imagent import Imagent, Canvas
import glob
import random
import argparse

parser = argparse.ArgumentParser(
    description="""create a quarter radial pattern by sprinkling some bitmaps""")
parser.add_argument('output',
                    help='path to output, must end in .png')
args = parser.parse_args()

assert args.output.endswith(".png")

hojotas = [p for p in glob.glob('hojotas/*.png')]
hojitas = [p for p in glob.glob('hojitas/*.png')]
bones = [p for p in glob.glob('calacoid/bones/*.png')]
skeletons = [p for p in glob.glob('calacoid/calaquitas/*.png')]
skulls = [p for p in glob.glob('calacoid/craneos/*.png')]
flores = [p for p in glob.glob('calacoid/flores/*.png')]
florecitas = [p for p in glob.glob('calacoid/florecitas/*.png')]


# hojotas 15
# hojitas 3
# bones 18
# skeletons 44
# flores 52
# florecitas 51


c = Canvas(h=6000, w=6000)

for i in range(2):
    for h in skulls:
        img = Imagent(h)
        img.im = img.tilt(c.sprinkle_heading)    
        c.sprinkle(img, step=600)

c.sprinkle_heading = 20
for h in hojotas:
    img = Imagent(h)
    img.im = img.tilt(c.sprinkle_heading)
    c.sprinkle(img, step=600)

c.sprinkle_heading = 60
for h in skeletons:
   img = Imagent(h)
   img.im = img.tilt(random.randint(0, 180))
   c.sprinkle(img, step=600)

c.sprinkle_heading = 10
for i in range(2):
    for h in flores:
        img = Imagent(h)
        img.im = img.tilt(random.randint(0, 180))
        c.sprinkle(img, step=500)

c.sprinkle_heading = 25
for i in range(2):
    for h in florecitas:
        img = Imagent(h)
        img.im = img.tilt(random.randint(0, 180))
        c.sprinkle(img, step=40)


c.sprinkle_heading = 25
for i in range(2):
    for h in bones:
        img = Imagent(h)
        img.im = img.tilt(random.randint(0, 180))
        c.sprinkle(img, step=40)
        
c.im.save(args.output)
