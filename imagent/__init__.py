from PIL import Image
import numpy as np
from random import randint, random

class Imagent(object):

    def __init__(self, path, x=0, y=0):
        self.im = Image.open(path)
        self.rotation = 0
        self.heading = 0
        self.x = x
        self.y = y

    def array(self):
        return np.array(self.im)

    def tilt(self, angle=-6):
        """
        rotate image clockwise by one degree
        """
        self.rotation += angle

        rotated = self.im.rotate(self.rotation,
                                 resample=Image.NEAREST,
                                 expand=True)
        bbox = rotated.getbbox()
        cut = rotated.crop(bbox)

        margin = int(np.sqrt(self.im.width**2 + self.im.height**2))
        
        H = bbox[2] - bbox[0] + margin
        W = bbox[3] - bbox[1] + margin
        a = np.zeros((H, W, 4),
                     dtype=np.uint8)

        trim = Image.fromarray(a,
                               mode="RGBA")
        trim.paste(cut, (0, 0))
        
        return trim

    def forward(self, steps):
        self.y += int(np.sin(np.radians(self.heading)) * steps)
        self.x += int(np.cos(np.radians(self.heading)) * steps)



class Canvas(object):

    def __init__(self, w=5000, h=5000):
        array = np.zeros((h, w, 4),
                         dtype=np.uint8)
        self.im = Image.fromarray(array,
                                  mode="RGBA")
        self.sprinkle_heading = 45

        
    def render(self, imagent):
        self.im.paste(imagent.im,
                      (imagent.x, imagent.y),
                      mask=imagent.im)


    def overlap(self, imagent):
        m = imagent.array() > 0
        c = np.array(self.im)

        h, w, d = m.shape
        cm = c[imagent.y:imagent.y+h, imagent.x:imagent.x+w] > 0
        if m.shape == cm.shape:
            return np.logical_and(m, cm).any()
        else:
            return True

    def contains(self, imagent):
        left, upper, right,  lower = imagent.im.getbbox()
        width = right - left
        height = lower - upper
        if ((imagent.x + width) <= self.im.width
            and
            (imagent.y + height) <= self.im.height):
            return True
        else:
            return False
        
    
    def sprinkle(self, imagent, step=10):
        imagent.heading = self.sprinkle_heading

        while True:

            if self.contains(imagent):
                if self.overlap(imagent):
                    imagent.forward(step)
                else:
                    self.render(imagent)
                    break
            else:
                if self.sprinkle_heading >= 90:
                    self.sprinkle_heading = 0
                else:
                    self.sprinkle_heading += 15
                break

        
