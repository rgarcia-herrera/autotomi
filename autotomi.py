#!/usr/bin/env python

from imagent import Imagent, Canvas
import glob
import random
import argparse

parser = argparse.ArgumentParser(
    description="""create a quarter radial pattern by sprinkling some bitmaps""")
parser.add_argument('output',
                    help='path to output, must end in .png')
args = parser.parse_args()

assert args.output.endswith(".png")

hojotas = [p for p in glob.glob('hojotas/*.png')]
hojitas = [p for p in glob.glob('hojitas/*.png')]
dinos = [p for p in glob.glob('dinos/*.png')]


c = Canvas(h=6000, w=6000)

for h in range(40):
    img = Imagent(random.choice(dinos))
    img.im = img.tilt(img.heading)
    c.sprinkle(img, step=600)

c.sprinkle_heading = 45
for h in range(20):
    img = Imagent(random.choice(hojotas))
    img.im = img.tilt(img.heading)
    c.sprinkle(img, step=600)

c.sprinkle_heading = 45
for h in range(40):
   img = Imagent(random.choice(dinos))
   img.im = img.tilt(random.randint(0, 180))
   c.sprinkle(img, step=600)

c.sprinkle_heading = 45
for h in range(200):
    img = Imagent(random.choice(hojitas))
    img.im = img.tilt(random.randint(0, 180))
    c.sprinkle(img, step=50)

c.im.save(args.output)
